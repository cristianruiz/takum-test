'use strict';
angular.module('takum')
        .controller('CreateCategoryController', function ($scope, Category, $location, messageCenterService) {
            $scope.category = new Category;

            $scope.error_msg = '';

            $scope.save = function () {
                console.log($scope.category);
                $scope.category.$save().then(function(data){
                    messageCenterService.add('success', 'Category successfully saved.', { status: messageCenterService.status.next, timeout: 5000 });
                    $location.url('/categories');
                }, function(error){
                    console.log(error);
                    messageCenterService.add('danger', 'Error! category could not be saved.', { status: messageCenterService.status.next, timeout: 5000 });
                });
            };

            $scope.validateField = function (field) {
                return field.$invalid && field.$touched;
            };
        });
