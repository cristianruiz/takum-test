'use strict';
angular.module('takum')
        .controller('CreateLanguageController', function ($scope, Language, $location, messageCenterService) {
            $scope.language = new Language;

            $scope.error_msg = '';

            $scope.save = function () {
                console.log($scope.language);
                $scope.language.$save().then(function(data){
                    messageCenterService.add('success', 'Language successfully saved.', { status: messageCenterService.status.next, timeout: 5000 });
                    $location.url('/languages');
                }, function(error){
                    console.log(error);
                    messageCenterService.add('danger', 'Error! language could not be saved.', { status: messageCenterService.status.next, timeout: 5000 });
                });
            };

            $scope.validateField = function (field) {
                return field.$invalid && field.$touched;
            };
        });

