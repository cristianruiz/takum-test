'use strict';
// Declare app level module which depends on views, and components
angular.module('takum')
        .controller('ListProductController', function ($scope, Product, messageCenterService, Category) {
            
            $scope.getProducts = function(){
                var condition = {};
                if($scope.categories && $scope.categories.selected)
                    condition = {category: $scope.categories.selected};
                $scope.products = Product.query(condition);
            };
            
            Category.get().$promise.then(function (data) {
                $scope.categories = data.data;                
            });
            
    
            $scope.removeProduct = function(product){
                var x = confirm('Do you really want to remove this product?');
                if(x){
                    $scope.products.$delete({id: product.id}).then(function(data){
                        messageCenterService.add('success', 'Product successfully removed.', { status: messageCenterService.status.next, timeout: 5000 });
                        $scope.getProducts();
                    }, function(error){
                        messageCenterService.add('danger', 'Error! product could not be removed.', { status: messageCenterService.status.next, timeout: 5000 });
                        $scope.getProducts();
                        console.log(error);
                    });
                }
            };
            
            
            
            $scope.getProducts();
        });