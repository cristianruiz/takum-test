'use strict';
angular.module('takum')
        .controller('EditCategoryController', function ($scope, Category, $routeParams, $location, $http, messageCenterService) {
            var category_id = $routeParams.id;

            var categoryResource = null;

            Category.get({id: category_id}).$promise.then(function (data) {
                categoryResource = data;
                $scope.category = categoryResource.data;
            });

            $scope.error_msg = '';

            $scope.save = function () {

                $http({
                    method: "PUT",
                    url: basePath + "/rest/category/" + $scope.category.id,
                    params: {
                        action: "add"
                    },
                    data: {
                        data: $scope.category
                    }
                }).then(function(data){
                    messageCenterService.add('success', 'Category successfully saved.', { status: messageCenterService.status.next, timeout: 5000 });
                    $location.url('/categories');
                }).catch(function(error){
                    console.log(error);
                    messageCenterService.add('danger', 'Error! category could not be saved.', { status: messageCenterService.status.next, timeout: 5000 });
                });
            };

            $scope.validateField = function (field) {
                return field.$invalid && field.$touched;
            };

        });