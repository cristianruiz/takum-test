'use strict';
angular.module('takum')
        .controller('EditProductController', function ($scope, Product, Language, Category, $routeParams, $location, $http, messageCenterService) {
            var product_id = $routeParams.id;
            var productResource = null;

            Product.get({id: product_id}).$promise.then(function (data) {
                productResource = data;
                $scope.product = productResource.data;
            });

            Language.get().$promise.then(function (data) {
                $scope.languages = data.data;
            });

            Category.get().$promise.then(function (data) {
                $scope.categories = data.data;
            });

            $scope.error_msg = '';

            $scope.addProductInfo = function () {
                $scope.product.info.push({language_id: $scope.languages[0].id, name: null, description: null});
            };

            $scope.removeProductInfo = function (idx) {
                if ($scope.product.info.length > 1)
                    $scope.product.info.splice(idx, 1);
            };

            $scope.save = function () {
                $http({
                    method: "PUT",
                    url: basePath + "/rest/product/" + $scope.product.id,
                    params: {
                        action: "add"
                    },
                    data: {
                        data: $scope.product
                    }
                }).then(function(data){
                    messageCenterService.add('success', 'Product successfully saved.', { status: messageCenterService.status.next, timeout: 5000 });
                    $location.url('/');
                }).catch(function(error){
                    console.log(error);
                    messageCenterService.add('danger', 'Error! product could not be saved.', { status: messageCenterService.status.next, timeout: 5000 });
                });
            };

            $scope.validateField = function (field) {
                return field.$invalid && field.$touched;
            };

        });