'use strict';
// Declare app level module which depends on views, and components
angular.module('takum')
        .controller('ListCategoryController', function ($scope, Category, messageCenterService) {
            
            $scope.getCategories = function(){
                $scope.categories = Category.query();
            };
            
            $scope.removeCategory = function(category){
                var x = confirm('Do you really want to remove this category?');
                if(x){
                    $scope.categories.$delete({id: category.id}).then(function(data){
                        messageCenterService.add('success', 'Category successfully removed.', { status: messageCenterService.status.next, timeout: 5000 });
                        $scope.getCategories();
                    }, function(error){
                        messageCenterService.add('danger', 'Error! category could not be removed.', { status: messageCenterService.status.next, timeout: 5000 });
                        $scope.getCategories();
                        console.log(error);
                    });
                }
            };
            
            
            
            $scope.getCategories();
        });