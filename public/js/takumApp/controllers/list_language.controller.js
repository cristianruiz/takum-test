'use strict';
// Declare app level module which depends on views, and components
angular.module('takum')
        .controller('ListLanguageController', function ($scope, Language, messageCenterService) {
            
            $scope.getLanguages = function(){
                $scope.languages = Language.query();
            };
            
            $scope.removeLanguage = function(language){
                var x = confirm('Do you really want to remove this language?');
                if(x){
                    $scope.languages.$delete({id: language.id}).then(function(data){
                        messageCenterService.add('success', 'Language successfully removed.', { status: messageCenterService.status.next, timeout: 5000 });
                        $scope.getLanguages();
                    }, function(error){
                        messageCenterService.add('danger', 'Error! language could not be removed.', { status: messageCenterService.status.next, timeout: 5000 });
                        $scope.getLanguages();
                        console.log(error);
                    });
                }
            };
            
            
            
            $scope.getLanguages();
        });