'use strict';
angular.module('takum')
        .controller('CreateProductController', function ($scope, Language, Category, Product, $location, messageCenterService) {
            $scope.product = new Product;

            Language.get().$promise.then(function (data) {
                $scope.languages = data.data;
                if ($scope.languages && $scope.languages[0]) {
                    $scope.product.info = [
                        {language_id: $scope.languages[0].id, title: null, description: null},
                    ];
                }
            });

            Category.get().$promise.then(function (data) {
                $scope.categories = data.data;
                if ($scope.categories && $scope.categories[0])
                    $scope.product.category_id = $scope.categories[0].id;
            });

            $scope.error_msg = '';

            $scope.addProductInfo = function () {
                $scope.product.info.push({language_id: $scope.languages[0].id, name: null, description: null});
            };

            $scope.removeProductInfo = function (idx) {
                if ($scope.product.info.length > 1)
                    $scope.product.info.splice(idx, 1);
            };

            $scope.save = function () {
                console.log($scope.product);
                $scope.product.$save().then(function(data){
                    messageCenterService.add('success', 'Product successfully saved.', { status: messageCenterService.status.next, timeout: 5000 });
                    $location.url('/');
                }, function(error){
                    console.log(error);
                    messageCenterService.add('danger', 'Error! product could not be saved.', { status: messageCenterService.status.next, timeout: 5000 });
                });
            };

            $scope.validateField = function (field) {
                return field.$invalid && field.$touched;
            };
        });

