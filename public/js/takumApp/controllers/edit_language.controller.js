'use strict';
angular.module('takum')
        .controller('EditLanguageController', function ($scope, Language, $routeParams, $location, $http, messageCenterService) {
            var language_id = $routeParams.id;
            var languageResource = null;

            Language.get({id: language_id}).$promise.then(function (data) {
                languageResource = data;
                $scope.language = languageResource.data;
            });

            $scope.error_msg = '';

            $scope.save = function () {

                $http({
                    method: "PUT",
                    url: basePath + "/rest/language/" + $scope.language.id,
                    params: {
                        action: "add"
                    },
                    data: {
                        data: $scope.language
                    }
                }).then(function(data){
                    messageCenterService.add('success', 'Language successfully saved.', { status: messageCenterService.status.next, timeout: 5000 });
                    $location.url('/languages');
                }).catch(function(error){
                    console.log(error);
                    messageCenterService.add('danger', 'Error! language could not be saved.', { status: messageCenterService.status.next, timeout: 5000 });
                });
            };

            $scope.validateField = function (field) {
                return field.$invalid && field.$touched;
            };

        });