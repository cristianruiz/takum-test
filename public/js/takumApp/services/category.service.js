'use strict';
// Declare app level module which depends on views, and components
angular.module('takum')
        .factory('Category', function ($resource) {
            var Category = $resource(basePath + '/rest/category/:id', {id: '@id', },
                    {
                        query: {
                            isArray: false
                        },
                        get: {
                            method: 'GET',
                            params: {id: '@id'},
                        },
                        update: {
                            method: 'PUT',
                            params: {id: '@id'}
                        },
                    });

            return Category;
        });