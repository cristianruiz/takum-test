'use strict';
// Declare app level module which depends on views, and components
angular.module('takum')
        .factory('Language', function ($resource) {
            var Language = $resource(basePath + '/rest/language/:id', {id: '@id', },
                    {
                        query: {
                            isArray: false
                        },
                        get: {
                            method: 'GET',
                            params: {id: '@id'},
                        },
                        update: {
                            method: 'PUT',
                            params: {id: '@id'}
                        },
                    });

            return Language;
        });