'use strict';
// Declare app level module which depends on views, and components
angular.module('takum')
        .factory('Product', function ($resource) {
            var Product = $resource(basePath + '/rest/product/:id', {id: '@id', },
                    {
                        query: {
                            isArray: false
                        },
                        get: {
                            method: 'GET',
                            params: {id: '@id'},
                        },
                        update: {
                            method: 'PUT',
                            params: {id: '@id'}
                        },
                    });

            return Product;
        });