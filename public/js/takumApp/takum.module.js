'use strict';
// Declare app level module which depends on views, and components
angular.module('takum', ['ngRoute', 'ngResource', 'MessageCenterModule'])
        .config(function ($routeProvider) {
            $routeProvider
                    .when('/', {
                        templateUrl: 'js/takumApp/views/list_product.html',
                        controller: 'ListProductController'
                    })
                    .when('/products', {
                        templateUrl: 'js/takumApp/views/list_product.html',
                        controller: 'ListProductController'
                    })
                    .when('/categories', {
                        templateUrl: 'js/takumApp/views/list_category.html',
                        controller: 'ListCategoryController'
                    })
                    .when('/languages', {
                        templateUrl: 'js/takumApp/views/list_language.html',
                        controller: 'ListLanguageController'
                    })
                    .when('/products/create', {
                        templateUrl: 'js/takumApp/views/create_product.html',
                        controller: 'CreateProductController'
                    })
                    .when('/categories/create', {
                        templateUrl: 'js/takumApp/views/create_category.html',
                        controller: 'CreateCategoryController'
                    })
                    .when('/languages/create', {
                        templateUrl: 'js/takumApp/views/create_language.html',
                        controller: 'CreateLanguageController'
                    })
                    .when('/products/edit/:id', {
                        templateUrl: 'js/takumApp/views/edit_product.html',
                        controller: 'EditProductController'
                    })
                    .when('/categories/edit/:id', {
                        templateUrl: 'js/takumApp/views/edit_category.html',
                        controller: 'EditCategoryController'
                    })
                    .when('/languages/edit/:id', {
                        templateUrl: 'js/takumApp/views/edit_language.html',
                        controller: 'EditLanguageController'
                    })
                    .otherwise({
                        redirectTo: '/'
                    });
        });
