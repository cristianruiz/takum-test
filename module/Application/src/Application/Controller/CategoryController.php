<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Application\Document\Category;

class CategoryController extends AbstractRestfulController {

    /**
     * List categories
     * @return JsonModel
     */
    public function getList() {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $data = $dm->createQueryBuilder('Application\Document\Category')
                ->getQuery()
                ->execute();

        $categories = array();
        foreach ($data as $row){   
            $categories[] = $this->serializer($row);
        }
        
        return new JsonModel(array(
            'data' => $categories
        ));
    }

    /**
     * Get details of single category
     * @return JsonModel
     */
    public function get($id) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $category = $dm->find('Application\Document\Category', $id);

        return new JsonModel(array(
            'data' => $this->serializer($category)
        ));
    }


    /**
     * Create a new category
     * @param type $data
     * @return JsonModel
     */
    public function create($data) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');

        $category = new Category();
        $category->setName($data['name']);

        $dm->persist($category);
        $dm->flush();

        return new JsonModel(array(
            'data' => $this->serializer($category)
        ));
    }

    /**
     * Edit a existing category
     * @param type $data
     * @return JsonModel
     */
    public function update($id, $data) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $category = $dm->find('Application\Document\Category', $id);

        $data = $data['data'];

        $category->setName($data['name']);

        $dm->persist($category);
        $dm->flush();

        return new JsonModel(array(
            'data' => $this->serializer($category)
        ));
    }

    /**
     * Delete a category
     * @param type $id
     * @return JsonModel
     */
    public function delete($id) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $category = $dm->find('Application\Document\Category', $id);
        $dm->remove($category);
        $dm->flush();

        return new JsonModel(array(
            'data' => true,
        ));
    }

    /**
     * Category Data
     * @param \Application\Document\Category $model
     * @return Array
     */
    private function serializer(\Application\Document\Category $model) {

        $category = [
            'id' => $model->getId(),
            'name' => $model->getName()
        ];

        return $category;
    }
}
