<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Application\Document\Product;
use Application\Document\ProductInfo;

class ProductController extends AbstractRestfulController {

    /**
     * List products
     * @return JsonModel
     */
    public function getList() {

        $category = $this->getRequest()->getQuery('category');

        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $data = $dm->createQueryBuilder('Application\Document\Product');
        if ($category) {
            $data = $data->field('category.$id')->equals(new \MongoId($category));
        }

        $data = $data->getQuery()->execute();

        $products = array();
        foreach ($data as $row) {
            $products[] = $this->serializer($row);
        }

        return new JsonModel(array(
            'data' => $products
        ));
    }

    /**
     * Get details of single product
     * @return JsonModel
     */
    public function get($id) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $product = $dm->find('Application\Document\Product', $id);

        return new JsonModel(array(
            'data' => $this->serializer($product)
        ));
    }

    /**
     * Create a new product
     * @param type $data
     * @return JsonModel
     */
    public function create($data) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');

        $product = new Product();

        if (isset($data['category_id']) && $data['category_id']) {
            $category = $dm->find('Application\Document\Category', $data['category_id']);

            if ($category)
                $product->setCategory($category);
        }

        foreach ($data['info'] as $row) {
            $info = new ProductInfo();
            if (isset($row['language_id']) && $row['language_id']) {
                $language = $dm->find('Application\Document\Language', $row['language_id']);
                if ($language)
                    $info->setLanguage($language);
            }
            $info->setTitle($row['title']);
            $info->setDescription($row['description']);
            $product->setProductInfo($info);
        }

        $dm->persist($product);
        $dm->flush();

        return new JsonModel(array(
            'data' => $this->serializer($product)
        ));
    }

    /**
     * Edit a existing product
     * @param type $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $product = $dm->find('Application\Document\Product', $id);

        $data = $data['data'];

        if (isset($data['category_id']) && $data['category_id']) {
            $category = $dm->find('Application\Document\Category', $data['category_id']);

            if ($category)
                $product->setCategory($category);
        }

        $product->clearProductInfo();

        foreach ($data['info'] as $row) {
            $info = new ProductInfo();
            if (isset($row['language_id']) && $row['language_id']) {
                $language = $dm->find('Application\Document\Language', $row['language_id']);
                if ($language)
                    $info->setLanguage($language);
            }
            $info->setTitle($row['title']);
            $info->setDescription($row['description']);
            $product->setProductInfo($info);
        }

        $dm->persist($product);
        $dm->flush();

        return new JsonModel(array(
            'data' => $this->serializer($product)
        ));
    }

    /**
     * Delete a product
     * @param type $id
     * @return JsonModel
     */
    public function delete($id) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $product = $dm->find('Application\Document\Product', $id);
        $dm->remove($product);
        $dm->flush();

        return new JsonModel(array(
            'data' => true,
        ));
    }

    /**
     * Product Data
     * @param \Application\Document\Product $model
     * @return Array
     */
    private function serializer(\Application\Document\Product $model) {

        $category = $model->getCategory();

        $product = [
            'id' => $model->getId(),
            'category_id' => ($category) ? $category->getId() : null,
            'category_name' => ($category) ? $category->getName() : null,
            'info' => []
        ];

        foreach ($model->getProductInfo() as $info) {
            $lang = $info->getLanguage();
            $product['info'][] = [
                'language_id' => ($lang) ? $lang->getId() : null,
                'language_name' => ($lang) ? $lang->getName() : null,
                'title' => $info->getTitle(),
                'description' => $info->getDescription()
            ];
        }

        return $product;
    }

}
