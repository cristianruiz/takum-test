<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Application\Document\Language;

class LanguageController extends AbstractRestfulController {

    /**
     * List languages
     * @return JsonModel
     */
    public function getList() {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $data = $dm->createQueryBuilder('Application\Document\Language')
                ->getQuery()
                ->execute();

        $languages = array();
        foreach ($data as $row){            
            $languages[] = $this->serializer($row);
        }
        
        return new JsonModel(array(
            'data' => $languages
        ));
    }

    /**
     * Get details of single language
     * @return JsonModel
     */
    public function get($id) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $language = $dm->find('Application\Document\Language', $id);

        return new JsonModel(array(
            'data' => $this->serializer($language)
        ));
    }


    /**
     * Create a new language
     * @param type $data
     * @return JsonModel
     */
    public function create($data) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');

        $language = new Language();
        $language->setName($data['name']);

        $dm->persist($language);
        $dm->flush();

        return new JsonModel(array(
            'data' => $this->serializer($language)
        ));
    }

    /**
     * Edit a existing language
     * @param type $data
     * @return JsonModel
     */
    public function update($id, $data) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $language = $dm->find('Application\Document\Language', $id);

        $data = $data['data'];

        $language->setName($data['name']);

        $dm->persist($language);
        $dm->flush();

        return new JsonModel(array(
            'data' => $this->serializer($language)
        ));
    }

    /**
     * Delete a language
     * @param type $id
     * @return JsonModel
     */
    public function delete($id) {
        $dm = $this->getServiceLocator()->get('doctrine.documentmanager.odm_default');
        $language = $dm->find('Application\Document\Language', $id);
        $dm->remove($language);
        $dm->flush();

        return new JsonModel(array(
            'data' => true,
        ));
    }

    /**
     * Language Data
     * @param \Application\Document\Language $model
     * @return Array
     */
    private function serializer(\Application\Document\Language $model) {

        $language = [
            'id' => $model->getId(),
            'name' => $model->getName()
        ];

        return $language;
    }
}
