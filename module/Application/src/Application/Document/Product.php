<?php
namespace Application\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="product") */
class Product
{
    /** @ODM\Id */
    private $id;
    
    /** @ODM\ReferenceOne(targetDocument="Application\Document\Category") */
    private $category;
    
    /** @ODM\EmbedMany(targetDocument="Application\Document\ProductInfo") */
    private $productInfo;
    
    public function __construct() { 
        $this->productInfo = new ArrayCollection();        
    }
    
    /**
     * @return the $id
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * @return the $category
     */
    public function getCategory() { 
        return $this->category;         
    }
    
    /**
     * @return the $productInfo
     */
    public function getProductInfo() { 
        return $this->productInfo;         
    }

    /**
     * @param field_type $id
     */
    public function setId($id) {
        $this->id = $id;
    }    
    
    /**
     * @param Category $category
     */
    public function setCategory(Category $category) { 
        $this->category = $category;         
    }
    
    /**
     * @param ProductInfo $productInfo
     */
    public function setProductInfo(ProductInfo $productInfo) { 
        $this->productInfo[] = $productInfo;         
    }
    
    /**
     * 
     */
    public function clearProductInfo() { 
        $this->productInfo = [];         
    }

}