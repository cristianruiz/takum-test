<?php
namespace Application\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\EmbeddedDocument */
class ProductInfo
{
    /** @ODM\Id */
    private $id;
    
    /** @ODM\Field(type="string") */
    private $title;
    
    /** @ODM\Field(type="string") */
    private $description;
    
    /** @ODM\ReferenceOne(targetDocument="Application\Document\Language") */
    private $language;
    
    /**
     * @return the $id
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * @return the $title
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * @return the $description
     */
    public function getDescription() {
        return $this->description;
    }
    
    /**
     * @return the $language
     */
    public function getLanguage() { 
        return $this->language;         
    }

    /**
     * @param field_type $id
     */
    public function setId($id) {
        $this->id = $id;
    }
    
    /**
     * @param field_type $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }   
    
    /**
     * @param field_type $description
     */
    public function setDescription($description) {
        $this->description = $description;
    } 
    
    /**
     * @param Language $language
     */
    public function setLanguage(Language $language) { 
        $this->language = $language;         
    }

}